import sys

# need to override python's default 10**4 level of recursion,
# as otherwise the minSteps will not work with large input.

sys.setrecursionlimit(10**6)

# memoize already calculated results
MEMO = {}

# calculate minimum steps it takes to reduce a number to a 1
# by either substracting a 1 or dividing by 2 or 3
#
# this is a good place to use DP, however since the recursion
# will spread exponentially, calculating same result many times,
# it will benefit MASSIVELY from memoization
#
# minSteps(6) will go like this
#               6
#             / | \
#           5   3   2
#         /    / \  / \
#        4     2  1 1  1
#       / \   / \
#      3    2 1  1
#     / \  / \
#    2   1 1  1
#   / \
#  1   1
# so from this basic example you can see that 2 is calculated 4 times,
# 3 is calculated 2 times. as the input increasing the tree will expand
# and slow down exponentially.
#
# thanks Sam for explanation: https://www.youtube.com/watch?v=f2xi3c1S95M


def min_steps(num):
    if num == 1:
        return 0
    if num in MEMO:
        return MEMO[num]
    result = min_steps(num - 1)
    if num % 3 == 0:
        result = min(result, min_steps(num / 3))
    elif num % 2 == 0:
        result = min(result, min_steps(num / 2))
    MEMO[num] = result + 1
    return MEMO[num]


print(min_steps(10000))
