def longest_palindrome(s: str) -> str:
    longest = s[0]
    best_len = 0
    length = len(s)
    for mid in range(length):
        # check odd palindromes (e.g. 'aba') and expand
        for offset in range(1, length):
            if mid - offset < 0 or mid + offset >= length:
                break
            if s[mid - offset] != s[mid + offset]:
                break
            new_len = offset * 2 + 1
            if new_len > best_len:
                best_len = new_len
                longest = s[mid-offset : mid + offset + 1]

        # check even palindromes (e.g. 'aa') and expand
        for offset in range(1, length):
            if mid - offset < 0 or mid + offset - 1 >= length:
                break
            if s[mid - offset] != s[mid + offset -1]:
                break
            new_len = offset * 2
            if new_len > best_len:
                best_len = new_len
                longest = s[mid-offset : mid+offset]

    return longest

if __name__ == '__main__':
    print(longest_palindrome("cmmrracelnclsbtdmuxtfiyahrvxuwreyorosyqapfpnsntommsujibzwhgugwtvxsdsltiiyymiofbslwbwevmjrsbbssicnxptvwmsmiifypoujftxylpyvirfueagprfyyydxeiftathaygmolkcwoaavmdmjsuwoibtuqoewaexihispsshwnsurjopdwttlzyqdbkypvjsbuidsdnpgklhwfnqdvlffcysnxeywvwvblatmxbflnuykhfhjptenhcxqinomlwalvlezefqybpuepbnymzlruuirpiatqgjgcnfmrlzshauoxuoqopcikogfwpssjdeplytcapmujyvgtfmmolnuadpwblgmcaututcrwsqrlpaaqobjfnhudmsulztbdkxpfejavastxejtpbqfftdtcdhvtpbzfuqcwkxtldtjycreimiujtxudtmokcoebhodbkgkgxjzrgyuqhozqtidltodlkziyofdeszwiobkwesdijxbbagguxvofvtphqxgvidqfkljufgubjmjllxoanbizwtedykwmneaosopynzlzvrlqcmyaahdcagfatlhwtgqxsyxwjhexfiplwtrtydjzrsysrcwszlntwrpgfedhgjzhztffqnjotlfudvczwfkhuwmdzcqgrmfttwaxocohtuscdxwfvhcymjpkqexusdaccw"))
